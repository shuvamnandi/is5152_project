import csv
import datetime
import json
import os
import pandas as pd
import requests

## FourSquare API
EXPLORE_API_URL = 'https://api.foursquare.com/v2/venues/explore'
SEARCH_API_URL = 'https://api.foursquare.com/v2/venues/search'
VENUE_API_URL = 'https://api.foursquare.com/v2/venues/{venueId}'
API_VERSION_DATE = '20200301'
CLIENT_ID = 'VY4AHEEUUIHRC1MPM4CUIGFK0IMSIZZLO3ALI3WGNLFVU4X0'
CLIENT_SECRET = 'L3ZLPRNCOAI1FRS3UTESPHR4SPQQXLJXTV5ACRLSLRPGMHA0'
CITY = 'New York'
RESULTS_LIMIT = 1000
MIN_RADIUS = 200  # 200 metres
MAX_RADIUS = 1000  # 1000 metres
DISTANCE_IF_NOT_FOUND = 99999999.

## For dataset
DATA_SET_DIRECTORY = '/Users/shuvamnandi/PycharmProjects/NUS/IS5152_Project/resources/dataset/unique_listings/'
FILE_NAME = 'unique_listings_0_0_to_13514.csv'
DATA_SET_LAST_SCRAPED_INDEX = 3
DATA_SET_LATITUDE_INDEX = 48
DATA_SET_LONGITUDE_INDEX = 49
DATA_SET_DATE_TIME_FORMAT = '%Y-%m-%d'
DATA_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
UNIQUE_DATA_SET_LAST_SCRAPED_INDEX = 2
UNIQUE_LISTINGS_DATA_SET_LATITUDE_INDEX = 3
UNIQUE_LISTINGS_DATA_SET_LONGITUDE_INDEX = 4
DATA_SET_DATE_TIME_FORMAT = '%Y-%m-%d'
ll = '40.7580,-73.9855' # Times Square

## Queries to make
QUERY_METRO_STATION = 'Subway Station'
QUERY_BUS_STOP = 'Bus Stop'
QUERY_RESTAURANT = 'Restaurant'
QUERY_SHOPS = 'Super Market'
QUERY_ATTRACTIONS = 'Attraction'
QUERY_CONCATENATOR = '|'

QUERY_TYPE_SEARCH = 'search'
QUERY_TYPE_EXPLORE = 'explore'
SEARCH_RESPONSE_KEYWORD = 'venues'
EXPLORE_RESPONSE_KEYWORD = 'groups'

## Verify venues' categories to count them as valid
CATEGORY_RESTAURANT = ('Restaurant', )
CATEGORY_BUS_STOP = ('Bus Stop', )
CATEGORY_METRO_STATION = ('Metro Station', 'Metro', )
CATEGORY_SHOPS = ('Grocery Store', 'Supermarket', 'Convenience Store', 'Flea Market', 'Farmers Market',
                         'Market', 'Deli / Bodega', 'Deli / Bodega', 'Accessories Store')

QUERY_TO_CATEGORY_MAP = {QUERY_RESTAURANT: CATEGORY_RESTAURANT,
                         QUERY_BUS_STOP: CATEGORY_BUS_STOP,
                         QUERY_METRO_STATION: CATEGORY_METRO_STATION,
                         QUERY_SHOPS: CATEGORY_SHOPS}

def readLatituteLongitude(fileName):
    df = pd.read_csv(fileName, usecols=['last_modified', 'borough', 'latitude', 'longitude'])
    data1 = df.drop_duplicates(subset=['longitude'])
    data2 = df.drop_duplicates(subset=['latitude'])
    data3 = df.drop_duplicates(subset=['latitude', 'longitude'])
    print(df[(df['latitude'] == 40.766842) & (df['longitude'] == -73.912114)])


def exploreQueryInfo(latitude, longitude, query, radius_limit):
    params = dict(
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        v=API_VERSION_DATE,
        ll=latitude + "," + longitude,
        query=query,
        limit=RESULTS_LIMIT,
        radius=radius_limit,
    )
    resp = requests.get(url=EXPLORE_API_URL, params=params)
    return json.loads(resp.text)

def searchQueryInfo(latitude, longitude, query, radius_limit):
    params = dict(
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        v=API_VERSION_DATE,
        ll=latitude + "," + longitude,
        query='"'+query+'"',
        limit=RESULTS_LIMIT,
        radius=radius_limit,
    )
    resp = requests.get(url=SEARCH_API_URL, params=params)
    return json.loads(resp.text)


def getVenueCreatedAt(venueId):
    url = VENUE_API_URL.format(venueId=venueId)
    params = dict(
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        v=API_VERSION_DATE,
    )
    return datetime.datetime(1900, 1, 1) # To comment out later
    resp = requests.get(url=url, params=params)
    response = json.loads(resp.text)
    responseCode = response.get('meta', {}).get('code')
    if responseCode == 200:
        createdAtTypeLong = response.get('response', {}).get('venue').get('createdAt')
        if createdAtTypeLong:
            createdAt = datetime.datetime.fromtimestamp(createdAtTypeLong)
            return createdAt
    elif responseCode == 429:
        return datetime.datetime(1900, 1, 1)


def getVenueCountAndMinimumDistanceForQuery(listing_coordinates, query, listing_creation_time, radius=MAX_RADIUS, type=QUERY_TYPE_SEARCH):
    def _acceptVenueBasedOnCreationTimeAndListingTime(listing_creation_datetime_str, venue_creation_datetime):
        listing_creation_datetime = datetime.datetime.strptime(listing_creation_datetime_str, DATA_SET_DATE_TIME_FORMAT)
        defaultCount = 0
        defaultBool = False
        if venue_creation_datetime:
            if venue_creation_datetime <= listing_creation_datetime:
                defaultCount = 1
                defaultBool = venue_creation_datetime <= listing_creation_datetime
        return defaultCount, defaultBool

    count = alt_count = 0
    distances = [DISTANCE_IF_NOT_FOUND]
    if isinstance(query, tuple) or isinstance(query, list):
        queryToHit = QUERY_CONCATENATOR.join(query).replace(QUERY_CONCATENATOR, '', 0)
        count = {}
        alt_count = {}
        distances = {}
        for qType in query:
            count[qType] = 0
            alt_count[qType] = 0
            distances[qType] = [DISTANCE_IF_NOT_FOUND]
    else:
        queryToHit = query
    if type == QUERY_TYPE_SEARCH:
        response = searchQueryInfo(latitude=listing_coordinates.split(',')[0], longitude=listing_coordinates.split(',')[1],
                               query=queryToHit, radius_limit=radius)
        keyword = SEARCH_RESPONSE_KEYWORD
    elif type == QUERY_TYPE_EXPLORE:
        response = exploreQueryInfo(latitude=listing_coordinates.split(',')[0], longitude=listing_coordinates.split(',')[1],
                                   query=queryToHit, radius_limit=radius)
        keyword = EXPLORE_RESPONSE_KEYWORD

    responseCode = response.get('meta', {}).get('code')
    if responseCode == 200:
        if type == QUERY_TYPE_SEARCH:
            venues = response.get('response', {}).get(keyword)
        elif type == QUERY_TYPE_EXPLORE:
            venues = [venue.get('venue') for venue in response.get('response', {}).get(keyword, {})[0].get('items')]
        for venue in venues:
            if len(venue.get('categories')):
                venue_creation_datetime = getVenueCreatedAt(venue.get('id'))
                venueKeyInfo = _acceptVenueBasedOnCreationTimeAndListingTime(listing_creation_time, venue_creation_datetime)
                countToAdd = venueKeyInfo[0]
                venueAddedBeforeListingCreation = venueKeyInfo[0]
                if isinstance(query, tuple) or isinstance(query, list):
                    for qType in query:
                        if QUERY_TO_CATEGORY_MAP.get(qType):
                            # Don't count the query result if search category is not as per expectation set in QUERY_TO_CATEGORY_MAP
                            if venue.get('categories')[0].get('name') in QUERY_TO_CATEGORY_MAP.get(qType):
                                count[qType] += countToAdd
                                alt_count[qType] += 1
                                if venue.get('location', {}).get('distance') is not None and venueAddedBeforeListingCreation:
                                    distances[qType].append(venue.get('location', {}).get('distance'))
                        else:
                            count[qType] += countToAdd
                            alt_count[qType] += 1
                            if venue.get('location', {}).get('distance') is not None and venueAddedBeforeListingCreation:
                                distances[qType].append(venue.get('location', {}).get('distance'))
                else:
                    if QUERY_TO_CATEGORY_MAP.get(query):
                        # Don't count the query result if search category is not as per expectation set in QUERY_TO_CATEGORY_MAP
                        if venue.get('categories')[0].get('name') in QUERY_TO_CATEGORY_MAP.get(query):
                            count += countToAdd
                            alt_count += 1
                            if venue.get('location', {}).get('distance') is not None and venueAddedBeforeListingCreation:
                                distances.append(venue.get('location', {}).get('distance'))
                    else:
                        count += countToAdd
                        alt_count += 1
                        if venue.get('location', {}).get('distance') is not None and venueAddedBeforeListingCreation:
                            distances.append(venue.get('location', {}).get('distance'))
    elif responseCode in (429, 403):
        count = -1
        distances = [-DISTANCE_IF_NOT_FOUND]
    return count, distances


def extractLocationFeatures():
    '''
    Extract Location features from Four Square API
    '''
    with open(os.path.join(DATA_SET_DIRECTORY, FILE_NAME)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            with open(FILE_NAME.replace('.csv', '') + '_location_4square.csv', mode='a') as new_csv_file:
                csv_writer = csv.writer(new_csv_file, delimiter=",")
                if line_count == 0:
                    line_count += 1
                    print(line_count, )
                    print(f'Current Column names are {", ".join(row)}')
                    print(f'Column names are {", ".join(row)}')
                    new_headers = ['Subway_Count_Within_200m', 'Subway_Count_Within_1000m', 'Closest_Subway_Within_1000m',
                                   'Bus_Count_Within_200m', 'Bus_Count_Within_1000m', 'Closest_Bus_Stop_Within_1000m',
                                   'Restaurants_Count_Within_200m', 'Restaurants_Count_Within_1000m', 'Closest_Restaurant_Within_1000m',
                                   'Shops_Count_Within_200m', 'Shops_Count_Within_1000m', 'Closest_Shop_Within_1000m',
                                   'Attractions_Count_Within_200m', 'Attractions_Count_Within_1000m', 'Closest_Attraction_Within_1000m']
                    for header in new_headers:
                        row.append(header)
                    csv_writer = csv.writer(new_csv_file, delimiter=",")
                    csv_writer.writerow(row)

                elif line_count:
                    line_count += 1
                    listing_created_time = row[UNIQUE_DATA_SET_LAST_SCRAPED_INDEX]
                    listing_coordinates = ", ".join([row[UNIQUE_LISTINGS_DATA_SET_LATITUDE_INDEX],
                                                     row[UNIQUE_LISTINGS_DATA_SET_LONGITUDE_INDEX]])

                    queriesToRun = ( (QUERY_METRO_STATION, QUERY_TYPE_SEARCH),
                                     (QUERY_BUS_STOP, QUERY_TYPE_SEARCH),
                                     (QUERY_RESTAURANT, QUERY_TYPE_SEARCH),
                                     (QUERY_SHOPS, QUERY_TYPE_SEARCH),
                                     (QUERY_ATTRACTIONS, QUERY_TYPE_EXPLORE))
                    new_data = []
                    for (query, queryType) in queriesToRun:
                        count_200m, _ = getVenueCountAndMinimumDistanceForQuery(
                            listing_coordinates, query,
                            listing_created_time, radius=MIN_RADIUS, type=queryType)
                        count_1000m, closest_within_1000m = getVenueCountAndMinimumDistanceForQuery(
                            listing_coordinates, query,
                            listing_created_time, radius=MAX_RADIUS, type=queryType)
                        new_data.append(count_200m)
                        new_data.append(count_1000m)
                        new_data.append(min(closest_within_1000m))
                    print(line_count, ": Coordinates= ", listing_coordinates, " new data:", new_data)
                    for data in new_data:
                        row.append(data)
                    csv_writer.writerow(row)

# print (readLatituteLongitude(os.path.join( DATA_SET_DIRECTORY, FILE_NAME )))
extractLocationFeatures()